#!/usr/bin/env bats
#Test script that use the various calculato demo to launch functional tests

@test "FXLC: add " {
	run ./fxlc add 2 3
	[ "$status" -eq 0 ]
	[ "$output" = "2 + 3 = 5" ]
}

@test "FXLC: sub " {
	run ./fxlc sub 12 4
	[ "$status" -eq 0 ]
	[ "$output" = "12 - 4 = 8" ]
}

@test "FXLC: mul " {
	run ./fxlc mul 5 8
	[ "$status" -eq 0 ]
	[ "$output" = "5 x 8 = 40" ]
}

@test "FXLC: div " {
	run ./fxlc div 15 3
	[ "$status" -eq 0 ]
	[ "$output" = "15 / 3 = 5.00" ]
}

@test "FXLC: No argument" {
	run ./fxlc
	[ "$status" -eq 1 ]
}

@test "FXLC: Valgrind leak check" {
	run valgrind --leak-check=full --error-exitcode=1 ./fxlc add 2 3
	[ "$status" -eq 0 ]
}

@test "FXLS: bad argument" {
	run ./fxls -w
	[ "$status" -eq 1 ]
}

@test "FXLS: read a file" {
	run ./fxls < cmdtest00.txt
	[ "$status" -eq 0 ]
	[ "${lines[0]}" = "5 x 8 = 40" ]
	[ "${lines[1]}" = "12 + 28 = 40" ]
	[ "${lines[2]}" = "78 - 15 = 63" ]
	[ "${lines[3]}" = "29 / 3 = 9.67" ]
}

@test "FXLS: read a file with verbose" {
	run ./fxls -v < cmdtest00.txt
	[ "$status" -eq 0 ]
	[ "${lines[0]}" = "[0] > mul 5 8" ]
	[ "${lines[1]}" = "5 x 8 = 40" ]
	[ "${lines[2]}" = "[1] > add 12 28" ]
	[ "${lines[3]}" = "12 + 28 = 40" ]
	[ "${lines[4]}" = "[2] > sub 78 15" ]
	[ "${lines[5]}" = "78 - 15 = 63" ]
	[ "${lines[6]}" = "[3] > div 29 3" ]
	[ "${lines[7]}" = "29 / 3 = 9.67" ]
}

@test "FXLS: add" {
	run ./fxls < cmdtest01.txt
	[ "$status" -eq 0 ]
	[ "$output" = "3 + -8 = -5" ]	
}

@test "FXLS: add with verbose" {
	run ./fxls -v < cmdtest01.txt
	[ "$status" -eq 0 ]
	[ "${lines[0]}" = "[0] > add 3 -8" ]
	[ "${lines[1]}" = "3 + -8 = -5" ]
}

@test "FXLS: empty file" {
	run ./fxls -v < /dev/null
	[ "$status" -eq 0 ]
}

@test "FXLS: Valgrind leak check" {
	run valgrind --leak-check=full --error-exitcode=1 ./fxls < cmdtest00.txt
	[ "$status" -eq 0 ]
}

