# configuration for Windows 64 cross compilation
# Built from various tutorials:
# https://www.incredibuild.com/blog/cross-compile-with-cmake
# https://www.taricorp.net/2021/cmake-windows-cross/
# https://demensdeum.com/blog/2019/10/18/crosscompile-for-window-on-ubuntu-mingw-cmake/

# the name of the target operating system
set(CMAKE_SYSTEM_NAME Windows)

# which compilers to use
set(CMAKE_C_COMPILER x86_64-w64-mingw32-gcc)
set(CMAKE_CXX_COMPILER x86_64-w64-mingw32-g++)
set(CMAKE_RC_COMPILER x86_64-w64-mingw32-windres)
set(CMAKE_RANLIB x86_64-w64-mingw32-ranlib)

set(CMAKE_FIND_ROOT_PATH /usr/x86_64-w64-mingw32)

# adjust the default behavior of the find commands:
# search headers and libraries in the target environment
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)

# search programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# determine debugging information
set(CMAKE_BUILD_TYPE RelWithDebInfo)


