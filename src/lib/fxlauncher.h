/**
   FXLAUNCHER

   Main header file to be included by projects.

   @author Eric Pietrocupo
   @date   July 8th 2021
   @licence Apache License
*/

#ifndef FXLAUNCHER_H_INCLUDED
#define FXLAUNCHER_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif


/**

*/

/** Since the parameters of the functions are mostly business logic functions that might interact
   with a database, the type of possible arguments had been kept limited. This will determine
   which conversion function will be called if necessary. The constants below are used to determine
   the parameters used by the launched function. They somewhat match the datatype of SQLite.
*/

#define FXL_NONE 0      //special case to indicate no parameters. Generally a dummy for zero arg f(x)
#define FXL_TEXT 1
#define FXL_INTEGER 2
#define FXL_REAL 3

/** A union will be used to store the value it self in the launch tube. So the string passed
   in parameter will already have been converted successfully, so that the only thing required
   to do in the tube is access the parameters with the right variable name. It's up to the
   programmer to make sure that the field list is kept uptodate to ensure proper conversion.
*/

typedef union fxl_u_arg
{  char *text; //Pointer to a string of character
   long integer; //integer value
   double real;  //real value
}fxl_u_arg;

/** The following structure contains all the information about an argument including it's
    type and it's name. The type is important to ensure proper conversion, while the name
     is used to be displayed on the command line to the user.
*/

typedef struct fxl_s_arg_detail
{  char *name;
   int type;
}fxl_s_arg_detail;

/** All the function information will be stored in the structure below. It is important that this
structure is accurate and updated appropriately to make sure the launching tube does not crash
due to out of bound indexes or wrong data types.
*/

typedef struct fxl_s_function
{  char *command; // Command string key used to launch the associated function
   void (*fxtube)(fxl_u_arg*); // Pointer on the function tube to launch,
                               //all tube requires an parameter list.
   int nb_arg; // Number of arguments necessary in the function to launch
   char *description; //A simple documentation string to display to the user with help commands.
   fxl_s_arg_detail arg[]; //arguments expected by the method, this is a variable size array.
      //No need to NULL terminate since nb_arg is there. Make sure those values are uptodate.
}fxl_s_function;

/** Some engine setup are required before beign used. So the init routine must be called first.
    The functions will be stored into an internal array that will double in size each time the
    number of functions exceed the array size.

    Error code is returned in case of error. Error code are listed later in this document.

    The initial list size must be supplied. But exceeding the list size will auttomatically
    reallocate the memory.
*/

int fxl_init ( int initial_list_size );

/**
   So you will first have to declare your tubes that will be launching your business logic functions.
   And then create various structures that will detail how to use that tube. IMPORTANT: since type[]
   is a variable size array, your CANNOT create an array of fxl_s_function.

   A tube is basically a function that encapsulates your business logic function. Here is an
   example of tube for an integer adding function.

   void fxtube_add ( fxl_u_arg* arg)
   {  add( arg[0].integer,
           arg[1].integer);
   }

   the function structure could be initialised this way

   fxl_s_function f_add =
   {  .command="add",
      .fxtube = fxtube_add,
      .nb_arg = 2,
      .description = "Add 2 integers together and display the results.",
      .type = { FXL_INTEGER, FXL_INTEGER }
   };

   This would allow typing the command below:

   add 2 3

   Which would add 2 and 3 and display the result 5

   The add function is declared like this:

   void test_function_add ( int a, int b)
   {  printf ("%d + %d = %d\n", a, b, a+b );
   }

   It's important to note that the separator you intend to use between your parameters should not
   be part of the command name. So don't use set a command name "add client" if the space character
   is a delimiter.

   Since function must be added manually, either you create a struct pointer array, or create individual
   struct and add them using the methods below.

   IMPORTANT: The information you supply in the structure must match the parameter you are expecting.
   Giving the engine the wrong information, like specifying 2 parameters but reading a 3rd parameter
   from the structure will create a segmentation fault. You can also expect arg to be NULL if it's
   a zero parameter function you want to encapsulate.
*/

int fxl_add_function ( fxl_s_function *fx);

/** This method keeps uptodate an internal dynamic array resized on demmand that is kept sorted
    to allow binary search. It is recommended to populate the engine with all the function at the
    start of the program to avoid slowdown due to insertion.

    If two functions have the same name, the behavior is undefined, one of the two functions is
    going to get launched.

    Once all the functions has been added to the engine, you can now launch the functions
    by calling fxl_launch with the argument count and the list of arguments. Normally those
    argument string came from the main(), make sure you don't pass the name of the program itself.

    Or you can launch the the function using a plain command line string. strtok() will be used
    to decompose the string using the speficied delimiter. This means that the string will be
    modified. The _with_string() version will make a copy of your string to make tokenisation
    possible what ever the source of the string.

    IMPORTANT: if you want to make your own command line interpreter, remember that the carriage
    return (\n) is actually a delimiter.
*/

int fxl_launch_with_argv ( int argc, char **argv );
int fxl_launch_with_string ( char *args, const char *delimiter );

/** These functions above can return many error messages with it's returned integer. The list is
    defiled below.
*/

#define FXL_ERROR_UNKNOWN_TYPE           1
#define FXL_ERROR_FUNCTION_NOT_FOUND     2
#define FXL_ERROR_CANNOT_ALLOCATE_LIST   3
#define FXL_ERROR_CANNOT_REALLOCATE_LIST 4
#define FXL_ERROR_OTHER_MEMORY_PROBLEM   5
#define FXL_ERROR_COMMAND_STR_EMPTY      6
#define FXL_ERROR_INTERNAL_LIST_MISSING  7
#define FXL_ERROR_WRONG_NB_ARGUMENTS     8


/** Finally, when you program is about to close, you are required to dispose the resources that
    where allocated by the engine. So calling fxl_destroy() is necessary if you want to clean
    memory appropriately.
*/

void fxl_destroy ( void );

/*_______________________________ Command Interpreters ____________________________________

   There are various built-in command interpreters to save you the trouble of making one and
   reuse them in your applications. Command interpreters are used in situation where you
   intend to launch more than one function from a script or from an interactive command line.

   The first interpreter is the interactive command line. It will simply display a command
   prompt, read a string of text, and display the results. It will also add the help and exit
   commands.

   NOTE: Since fgets() is used, a buffer size must be supplied in parameter. 1 is added to the
   size to hold the \0

*/

void fxl_interactive_command_line ( char *prompt, int input_buffer_size );

/* There are two internal commands that are added without your consent in the interactive mode:

   HELP
   Syntax: help [command_name]
   Description: This command will detail the command information and show the description.

   EXIT
   Syntax: exit
   Description: This command will exit the command line and give you back the control on the
   program do to other things or destroy the engine.

   Then there is the non interactive command line that reads a file handle until end of file is
   encountered and display the results on stdout. It is possible to pass a file using fopen(),
   Or to pass stdin directly. It is possible to turn off the debuggin messages by setting
   'debug' at false.

*/

void fxl_file_command_line ( FILE *handle, int input_buffer_size, bool debug );

#ifdef __cplusplus
}
#endif


#endif // FXLAUNCHER_H_INCLUDED
