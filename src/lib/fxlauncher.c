/**
   FXLAUNCHER

   This is all the source code as there is not much requied

   @author Eric Pietrocupo
   @date   July 10th 2021
   @licence Apache License
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <fxlauncher.h>

//___________________________ Private definition _____________________________

fxl_s_function **_fxlist = NULL;
int _nb_function = 0;
int _list_size;
bool _exit_interactive_cli;

// Error List as String to display

char *errorstr[] = { "Success, no errors",
                  "Unknown argument types, valid types are: TEXT, INTEGER, REAL",
                  "Function not found",
                  "Cannot allocate internal function list",
                  "Cannot re-allocate internal function list",
                  "A memory problem occured",
                  "The command string is empty",
                  "The internal list of functions is missing",
                  "The number or arguments does not match the function requirements" };

//___________________________ Private methods ________________________________


/** Insertion function, insert at the end then move item up using a bubble sort style
    function because there is just 1 element to sort and most insertion should be done
    at the start of the program, so speed should not be an issue. There should be
    enough space in the list of functions;
*/
int _insert_function ( fxl_s_function *fx)
{  fxl_s_function *swapfx;
   _fxlist [_nb_function] = fx;

   int i = _nb_function;
   while ( i > 0 && strcmp ( _fxlist[i-1]->command, _fxlist[i]->command) > 0 )
   {  swapfx = _fxlist[i-1];
      _fxlist[i-1] = _fxlist[i];
      _fxlist[i] = swapfx;
      i--;
   }

   _nb_function++;
   return 0;
}

/** Return the function matching the command name if it exists.
    I designed my own binary search function since I could not make bsearch() to work.
*/
fxl_s_function *_find_function ( char *command )
{  int left = 0;
   int right = _nb_function - 1;

   while ( left <= right )
   {  int middle = (left + right) / 2; //should round down by default
      int cmp = strcmp (_fxlist[middle]->command , command );
      if ( cmp < 0 )
      {  left = middle + 1;
      }
      else if ( cmp > 0 )
      {  right = middle -1;
      }
      else return _fxlist[middle];
   }

   return NULL;

   //linear search
   /*for ( int i = 0; i < nb_function; i++ )
   {  if ( strcmp (command, fxlist[i]->command ) == 0 )
      {  return fxlist[i];
      }
   }
   return NULL;*/
}

/** Convert an single argument string to an union. return is made by value.
    There is no way to detect wrong convertions with this code.
*/
fxl_u_arg _convert_argument ( int type, char *arg )
{  fxl_u_arg argu = { .text=NULL };
   switch ( type )
   {  case FXL_INTEGER: argu.integer = strtol (arg, NULL, 10);
         break;
      case FXL_REAL: argu.real = strtod ( arg, NULL);
         break;
      case FXL_TEXT: argu.text = arg;
      break;
   }

   return argu;
}

void _print_command_description (fxl_s_function *f)
{  printf ("Usage: %s ", f->command );
   for ( int i = 0 ; i < f->nb_arg; i++ ) printf ("%s ", f->arg[i].name);
   printf ("\nParameters: \n");
   for ( int i = 0 ; i < f->nb_arg; i++ )
   {  printf ( "  %-20s ", f->arg[i].name);
      switch ( f->arg[i].type )
      {  case FXL_TEXT: printf ("TEXT\n");
         break;
         case FXL_INTEGER: printf ("INTEGER\n");
         break;
         case FXL_REAL: printf ("REAL\n");
         break;
      }
   }
   printf ("Description: %s\n", f->description );
}

void _print_all_commands ( void )
{  printf ( "available commands are: \n");
   int i = 0;
   for ( i = 0 ; i < _nb_function; i++ )
   {  printf ("%-40s ", _fxlist[i]->command);
      if ( i%2 == 1 ) printf ("\n");
   }
   /*if ( i%2 == 1 )*/ printf ("\n");
}

//___________________________ Internal commands ________________________
//NOTE: must be put here to have access to internal global variables
// I also put the function directly in the tube, since it's the only way to launch
// those functions

void _fxtube_exit ( fxl_u_arg* arg )
{  _exit_interactive_cli = true;
   printf ("Exiting interactive command line\n");
}

void _fxtube_help ( fxl_u_arg* arg )
{  fxl_s_function *f = _find_function(arg[0].text);
   if ( f == NULL)
   {  printf ("Command %s not found\n", f->command);
      _print_all_commands();
      return;
   }

   _print_command_description( f);
}

fxl_s_function _fx_exit =
{  .command="exit",
   .fxtube = _fxtube_exit,
   .nb_arg = 0,
   .description = "Exit the command line interface",
   .arg = { { "No arg", FXL_NONE } }
};

fxl_s_function _fx_help =
{  .command="help",
   .fxtube = _fxtube_help,
   .nb_arg = 1,
   .description = "Display information about a command",
   .arg = { { "command_name", FXL_TEXT } }
};

//___________________________ Public methods _________________________________

int fxl_init ( int initial_list_size )
{  if ( initial_list_size <= 0) initial_list_size = 16;
   _list_size = initial_list_size;
   _fxlist = malloc ( initial_list_size * sizeof (fxl_s_function*));
   if ( _fxlist == NULL ) return FXL_ERROR_CANNOT_ALLOCATE_LIST;

   return 0;
}


int fxl_add_function ( fxl_s_function *fx)
{
   if ( _fxlist == NULL ) return FXL_ERROR_INTERNAL_LIST_MISSING;

   if ( _nb_function >= _list_size )
   {  _list_size *= 2;
      fxl_s_function **tmptr = realloc ( _fxlist, _list_size * sizeof(fxl_s_function*) );
      if ( tmptr == NULL ) return FXL_ERROR_CANNOT_REALLOCATE_LIST;
      _fxlist = tmptr;
   }

   return _insert_function ( fx );
}

int fxl_launch_with_argv ( int argc, char **argv )
{  fxl_s_function *f = _find_function(argv[0]);
   if ( f == NULL) return FXL_ERROR_FUNCTION_NOT_FOUND;

   if ( argc - 1 != f->nb_arg ) return FXL_ERROR_WRONG_NB_ARGUMENTS;

   fxl_u_arg *arg = malloc ( sizeof (fxl_u_arg) * f->nb_arg );
   if ( arg == NULL && f->nb_arg > 0) return FXL_ERROR_OTHER_MEMORY_PROBLEM;

   for ( int i = 0 ; i < f->nb_arg ; i++ )
   {  arg[i] = _convert_argument( f->arg[i].type, argv[i+1]);
   }

   f->fxtube(arg);
   free (arg);

   return 0;
}

int fxl_launch_with_string ( char *args, const char *delimiter )
{  int i = 0;
   char *tmpstr;

   //String is duplicated to prevent crash from constant string
   char *argscopy = strdup ( args );
   if ( argscopy == NULL) return FXL_ERROR_OTHER_MEMORY_PROBLEM;

   tmpstr = strtok ( argscopy, delimiter );
   if ( tmpstr == NULL ) return FXL_ERROR_COMMAND_STR_EMPTY;

   fxl_s_function *f = _find_function(tmpstr);
   if ( f == NULL) return FXL_ERROR_FUNCTION_NOT_FOUND;

   fxl_u_arg *arg = malloc ( sizeof (fxl_u_arg) * f->nb_arg );
   if ( arg == NULL && f->nb_arg > 0) return FXL_ERROR_OTHER_MEMORY_PROBLEM;

   tmpstr = strtok ( NULL, delimiter);
   while ( tmpstr != NULL  )
   {  if ( i < f->nb_arg ) //makes sure there is no buffer overflow
      {  arg[i] = _convert_argument( f->arg[i].type, tmpstr);
      }
      tmpstr = strtok ( NULL, delimiter);
      i++;
   }

   if ( i != f->nb_arg ) return FXL_ERROR_WRONG_NB_ARGUMENTS;

   f->fxtube(arg);
   free (arg);
   free (argscopy);

   return 0;
}


void fxl_destroy ( void )
{  free (_fxlist);
}

void fxl_interactive_command_line ( char *prompt, int input_buffer_size )
{  char *in_buffer = (char*) malloc ( sizeof(char) * (input_buffer_size + 1 ));
   _exit_interactive_cli = false;

   if ( _find_function( "help") == NULL ) fxl_add_function( &_fx_help);
   if ( _find_function( "exit") == NULL ) fxl_add_function( &_fx_exit);

   while ( _exit_interactive_cli == false )
   {  printf ("%s", prompt);
      fgets (in_buffer, input_buffer_size, stdin );
      int error = fxl_launch_with_string( in_buffer, " \n");
      if ( error > 0 ) printf ("%s\n", errorstr[error] );
      if ( error == FXL_ERROR_COMMAND_STR_EMPTY
          || error == FXL_ERROR_FUNCTION_NOT_FOUND ) _print_all_commands();
      if ( error == FXL_ERROR_WRONG_NB_ARGUMENTS)
      {  printf("Type 'help command_name' for more usage information\n");
      }
   }

   free (in_buffer);
}

void fxl_file_command_line ( FILE *handle, int input_buffer_size, bool debug )
{  char *in_buffer = (char*) malloc ( sizeof(char) * (input_buffer_size + 1 ));
   int lineid = 0;

   fgets (in_buffer, input_buffer_size, handle );
   while ( feof( handle ) == 0 )
   {  if ( debug == true) printf ("[%d] > %s", lineid, in_buffer);
      int error = fxl_launch_with_string( in_buffer, " \n");
      if ( error > 0 && debug==true) printf ("[%d] ERROR : %s\n", lineid, errorstr[error] );
      lineid++;
      fgets (in_buffer, input_buffer_size, handle );
   }


   free (in_buffer);
}
