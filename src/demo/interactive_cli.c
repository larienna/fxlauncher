/**
   FXLAUNCHER

   Function launcher demo program: Interactive command line

   @author Eric Pietrocupo
   @date July 8th, 2021
   @licence Apache Licence
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <fxlauncher.h>
#include <calculator.h>

int main( int argc, char **args )
{  fxl_init(4);

   int error;
   if ( (error = fxl_add_function( &f_add )) != 0) return error;
   if ( (error = fxl_add_function( &f_subtract )) != 0) return error;
   if ( (error = fxl_add_function( &f_multiply )) != 0) return error;
   if ( (error = fxl_add_function( &f_divide )) != 0) return error;

   fxl_interactive_command_line ( "[fxl_calc] > ", 120);

   void fxl_destroy ( void );

   return 0;
}




