/**
   FXLAUNCHER: calculator demo

   This is all the functions and tubes required to make a simple calculator.

   @author Eric Pietrocupo
   @date   July 18th 2021
   @licence Apache License
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fxlauncher.h>
#include <calculator.h>


//example of multi parameter type, not used
int test_function_print ( int i, double f, char *s)
{  printf ("int: %d, float: %f, string: %s\n", i, f, s );
   return 0;
}

void test_function_add ( int a, int b)
{  printf ("%d + %d = %d\n", a, b, a+b );
}

void test_function_subtract ( int a, int b)
{  printf ("%d - %d = %d\n", a, b, a-b );
}

void test_function_multiply ( int a, int b)
{  printf ("%d x %d = %d\n", a, b, a*b );
}

void test_function_divide ( int a, int b)
{  if ( b == 0 )
   {  printf("Division by zero\n");
   }
   else
   {  printf ("%d / %d = %.2f\n", a, b, (float)a / (float)b );
   }
}

void fxtube_print ( fxl_u_arg* arg)
{  test_function_print( arg[0].integer,
                        arg[1].real,
                        arg[2].text);
}

void fxtube_add ( fxl_u_arg* arg)
{  test_function_add( arg[0].integer,
                      arg[1].integer);
}

void fxtube_subtract ( fxl_u_arg* arg)
{  test_function_subtract( arg[0].integer,
                      arg[1].integer);
}

void fxtube_multiply ( fxl_u_arg* arg)
{  test_function_multiply( arg[0].integer,
                      arg[1].integer);
}

void fxtube_divide ( fxl_u_arg* arg)
{  test_function_divide( arg[0].integer,
                         arg[1].integer);
}

//structure of functions to use as global variable

fxl_s_function f_print =
{  .command="print",
   .fxtube = fxtube_print,
   .nb_arg = 3,
   .arg = { {"Integer", FXL_INTEGER },
             {"Real", FXL_REAL},
             {"Text", FXL_TEXT} }
};
fxl_s_function f_add =
{  .command="add",
   .fxtube = fxtube_add,
   .nb_arg = 2,
   .arg = { {"Operand_A", FXL_INTEGER},
             {"Operand_B", FXL_INTEGER} },
   .description = "Add 2 integers together and display the results."
};
fxl_s_function f_subtract =
{  .command="sub",
   .fxtube = fxtube_subtract,
   .nb_arg = 2,
   .arg = { {"Operand_A", FXL_INTEGER},
             {"Operand_B", FXL_INTEGER} },
   .description = "Subtract the second integer from the first."
};
fxl_s_function f_multiply =
{  .command="mul",
   .fxtube = fxtube_multiply,
   .nb_arg = 2,
   .arg = { {"Operand_A", FXL_INTEGER},
             {"Operand_B", FXL_INTEGER} },
   .description = "Multiply 2 integers together"
};
fxl_s_function f_divide =
{  .command="div",
   .fxtube = fxtube_divide,
   .nb_arg = 2,
   .arg = { {"Dividend", FXL_INTEGER},
             {"Divider", FXL_INTEGER} },
   .description = "Divide the first integer by the second"
};

