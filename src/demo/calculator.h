/**
   FXLAUNCHER: calculator demo

   This is all the functions and tubes required to make a simple calculator.

   @author Eric Pietrocupo
   @date   July 18th 2021
   @licence Apache License
*/


#ifndef CALCULATOR_H_INCLUDED
#define CALCULATOR_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/*void test_function_add ( int a, int b);
void test_function_subtract ( int a, int b);
void test_function_multiply ( int a, int b);
void test_function_divide ( int a, int b);*/

/*void fxtube_add ( fxl_u_arg* arg);
void fxtube_subtract ( fxl_u_arg* arg);
void fxtube_multiply ( fxl_u_arg* arg);
void fxtube_divide ( fxl_u_arg* arg);*/

extern fxl_s_function f_add;
extern fxl_s_function f_subtract;
extern fxl_s_function f_multiply;
extern fxl_s_function f_divide;



#ifdef __cplusplus
}
#endif


#endif // CALCULATOR_H_INCLUDED
