/**
   FXLAUNCHER

   Function launcher demo program: Stdin reader

   read stdi to launch commands until end of file is reached.

   @author Eric Pietrocupo
   @date July 18th, 2021
   @licence Apache Licence
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <fxlauncher.h>
#include <calculator.h>

int main( int argc, char **argv )
{  fxl_init(4);
   bool verbose = false;

   if ( argc > 1)
   {  if ( strcmp (argv[1], "-v") == 0 ) verbose = true;
      else
      {  printf ("Usage: fxls [option]\n-v verbose: display command results\n");
         printf ("\nCommon usage consist in redirecting a file on stdin,\nelse the keyboard is read until CTRL+D is pressed\n");
         return 1;
      }
   }

   int error;
   if ( (error = fxl_add_function( &f_add )) != 0) return error;
   if ( (error = fxl_add_function( &f_subtract )) != 0) return error;
   if ( (error = fxl_add_function( &f_multiply )) != 0) return error;
   if ( (error = fxl_add_function( &f_divide )) != 0) return error;

   fxl_file_command_line( stdin, 120, verbose );

   void fxl_destroy ( void );

   return 0;
}




