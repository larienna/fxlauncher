/**
   FXLAUNCHER

   Function launcher demo program: single command

   Use program arguments to launch command

   @author Eric Pietrocupo
   @date July 18th, 2021
   @licence Apache Licence
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <fxlauncher.h>
#include <calculator.h>

int main( int argc, char **argv )
{  fxl_init(4);

   if ( argc < 2)
   {  printf ("Usage: fxlc command arg1 arg2 ...\n");
      return 1;
   }

   int error;
   if ( (error = fxl_add_function( &f_add )) != 0) return error;
   if ( (error = fxl_add_function( &f_subtract )) != 0) return error;
   if ( (error = fxl_add_function( &f_multiply )) != 0) return error;
   if ( (error = fxl_add_function( &f_divide )) != 0) return error;

   return fxl_launch_with_argv( argc -1, &argv[1]);

   void fxl_destroy ( void );
   return 0;
}




