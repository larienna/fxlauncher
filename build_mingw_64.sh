#!/bin/bash

#create directory if not exist
mkdir -p _build_mingw/win64

#remove any files previously created
cd _build_mingw/win64 && rm -r *

#build the project with cmake
cmake ../.. -DCMAKE_TOOLCHAIN_FILE=../../win64cc.cmake

#build all targets
make


