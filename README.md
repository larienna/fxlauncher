# F(x) Launcher (Function Launcher)

Author: Eric Pietrocupo  
Licence: Apache 2 License

This is a small library designed to launch internal business logic from the command line. The goal is to allow automated or manual functionnal test without using the user interface. It can also be used to prototype the internal business logic before the user interface is available.

## How does it works

The idea is to use a command as a token separated text string, convert the parameters in the right format and call the business logic function with those parameters. A registry of functions will be hold to know the possible functions that could be launched with a pointer on their launch tube function.

Launch tubes functions have uniform parameters that will be used to launch the real business logic function. So the business logic function will be encapsulated into their launch tube.

This system is more convenient for functional testing. It can be used to tests things that cannot be unit tested.

## Creating commands for your business logic

Here are some code example from the project. The demonstration program is a simple calculator program. Here is an example of business logic function:

```
void test_function_add ( int a, int b)
{  printf ("%d + %d = %d\n", a, b, a+b );
}
```

In order to launch this function, we have to create a tube that will encapsulate this method. Here is what the tube looks like:

```
void fxtube_add ( fxl_u_arg* arg)
{  test_function_add( arg[0].integer,
                      arg[1].integer);
}
```

The arg parameter is a union that contains all parameters passed to the command as `text`, `integer`, `real` format. The types are very similar to SQLite format. The number of arguments and the type of arguments will be defined in the structure below.

```
fxl_s_function f_add =
{  .command="add",
   .fxtube = fxtube_add,
   .nb_arg = 2,
   .arg = { {"Operand_A", FXL_INTEGER},
             {"Operand_B", FXL_INTEGER} },
   .description = "Add 2 integers together and display the results."
};
```

It is very important that his information is up to date. The tube function must have faith that the `fxl_u_arg` contains the right number of parameters in the right format. In order to avoid segmentation faults, the structure above must not have invalid information.

You can then create commands for any business logic routine in your applications. The command above can be executed this way:

```add 2 3```

the result will be

```2 + 3 = 5```

## Using the F(x) Launcher engine

There will be more intomation in the `fxlauncher.h` header file, but in a nutshell, here is how the engine must be used once your commands are programmed:

```
int main( int argc, char **argv )
{  fxl_init(4);

   int error;
   if ( (error = fxl_add_function( &f_add )) != 0) return error;
   
   return fxl_launch_with_argv( argc -1, &argv[1]);

   void fxl_destroy ( void );
}
```

This small program takes the command line arguments to launch a command. It makes sure to strip the argument zero since it will contain the program name.

First, `fxl_init()` must be called with the number of commands you are expecting to use. The list will be resized if more commands are added.

Second, `fxl_add_function()` can be used to add the command structure previously defined above. All commands must be added befor they can be launched. A binary search is used to find the matching command so it should be fast enough to handle a large amount of commands.

Third, `fxl_launch_with_argv()` will launch a command with separated arguments. You can use `fxl_launch_with_string()` if the command is in a simple string to separate it with a delimeter of your choice.

Finally, once you launched all your necessary commands, you must call `fxl_destroy()` to free memory allocated by the engine.

## Example programs

There is 3 different copies of the demonstration calculator program.

`fxli`: Interactive version that will open a command prompt where you can type commands then exit using the `exit` command. Typing an empty line will show you all the commands available. `help` can be used to display information on specific command like in `help add`  
`fxlc`: Command line command version that will take the command line arguments and launch the appropriate function like in the example above.  
`fxls`: Stdin reader version that will read lines of text from stdin and interpret them as command until end-of-file or CTRL+D is found. Option `-v` is available to show commands run with their ID. Useful for debugging logs.  

All those programs will be capable of launching calculator commands I have defined which are:

`add A B`: Add integer A and B together and display the results.  
`sub A B`: Subtract integer B from A and display the results.  
`mul A B`: Multiply integer A by B and display the results.  
`div A B`: Divide integer A by B and display the results. Of course, no division by zero is allowed.  

## How to build the software

I am using cmake to build the software, so Cmake must be installed on your computer. Tests requires Bats, but you will not need it to build the software. There is no other prerequisite since it's a fairly simple library.

Once ready, you can launch the build script that will create a `_build` directory and assemble the software:

`./buid.sh`

If you want to install the library on your computer and copy the demo program in the root of the project, you can launch the install script with administration rights:

`sudo ./install.sh`

Else the artifacts should be located here:

Demo programs: `_build/src/demo/`  
Static Libraries: `_build/src/lib/libfxl.a`

