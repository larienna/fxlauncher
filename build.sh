#!/bin/bash

#create directory if not exist
mkdir -p _build

#remove any files previously created
cd _build && rm -r *

#build the project with cmake
cmake ..

#build all targets
make

